package com.LaundryQ.sofyanajin.ugm;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class ListLayout extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.riwayat_list_layout);
    }

    public void gotoDetailLaundry(View view) {
        Intent detail = new Intent(this, ListLayout.class);
        startActivity(detail);
    }
}

